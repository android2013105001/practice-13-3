#include <jni.h>

#ifndef _Included_resolution_example6_zzeulki_practice133_MainActivity
#define _Included_resolution_example6_zzeulki_practice133_MainActivity


#ifdef __cplusplus
extern "C"{
#endif

jint Java_resolution_example6_zzeulki_practice133_MainActivity_add(JNIEnv * env, jobject obj,jint val1, jint val2){
    return (jint) val1+val2;
}
jint Java_resolution_example6_zzeulki_practice133_MainActivity_mul(JNIEnv * env, jobject obj,jint val1, jint val2){
    return (jint) val1*val2;
}
jint Java_resolution_example6_zzeulki_practice133_MainActivity_div(JNIEnv * env, jobject obj,jint val1, jint val2){
    return (jint) val1/val2;
}
#ifdef __cplusplus
}
#endif
#endif


