package resolution.example6.zzeulki.practice133;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("calculate");
    }

    public native int add(int x, int y);
    public native int mul(int x, int y);
    public native int div(int x, int y);

    TextView display;
    String operator = "+";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = (TextView) findViewById(R.id.display);
    }

    public void onclick(View v){
        switch(v.getId()){
            case R.id.btn1 :
                operator = "+";
                display.setText(operator);
                break;
            case R.id.btn2 :
                operator = "*";
                display.setText(operator);
                break;
            case R.id.btn3 :
                operator = "/";
                display.setText(operator);
                break;
            case R.id.btnCalc :
                TextView result = (TextView) findViewById(R.id.Result);
                EditText val1 = (EditText) findViewById(R.id.val1);
                EditText val2 = (EditText) findViewById(R.id.val2);

                int v1,v2,res = -1;
                try{
                    v1 = Integer.parseInt(val1.getText().toString());
                    v2 = Integer.parseInt(val2.getText().toString());
                }catch(NumberFormatException e){
                    v1 = 0;
                    v2 = 0;
                }

                if(operator.equals("+")){
                    res = add(v1,v2);
                }else if(operator.equals("*")){
                    res = mul(v1,v2);
                }else if(operator.equals("/")){
                    res = div(v1,v2);
                }

                result.setText(new Integer(res).toString());
                break;
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
